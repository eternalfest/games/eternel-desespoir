import bugfix.Bugfix;
import debug.Debug;
import game_params.GameParams;
import hf.Hf;
import merlin.Merlin;
import patchman.IPatch;
import patchman.Patchman;
import atlas.Atlas;
import eternal_despair.EternalDespair;

@:build(patchman.Build.di())
class Main {
  public static function main(): Void {
    Patchman.bootstrap(Main);
  }

  public function new(
    bugfix: Bugfix,
    debug: Debug,
    game_params: GameParams,
    atlasBoss: atlas.props.Boss,
    atlasDarkness: atlas.props.Darkness,
    atlas: atlas.Atlas,
    ed: EternalDespair,
    merlin: Merlin,
    patches: Array<IPatch>,
    hf: Hf
  ) {
    Patchman.patchAll(patches, hf);
  }
}
