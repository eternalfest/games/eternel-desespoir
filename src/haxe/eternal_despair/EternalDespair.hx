package eternal_despair;

import etwin.flash.MovieClip;
import hf.entity.Player;
import hf.Entity;
import hf.Hf;
import hf.mode.GameMode;
import merlin.IActionContext;
import merlin.IAction;
import patchman.IPatch;
import etwin.Obfu;
import patchman.Ref;

import sky_ui.SkyUI;
import better_script.Bombexpert;

class NoRemainingLives implements IAction {
    public var name(default, null): String = Obfu.raw("noRemainingLives");
    public var id: Int;
    public var isVerbose(default, null): Bool = false;

    public function new() {}

    public function run(ctx: IActionContext): Bool {
        for (player in ctx.getGame().getPlayerList()) {
            if (player.lives > 0)
                return false;
        }
        return true;
    }
}

@:build(patchman.Build.di())
class EternalDespair {
    private var nb_deaths: Array<Int>;
    private var map_screen_additional_sprites: Array<MovieClip>;

    private static var LEVEL_NAMES(default, never): Array<String> = [Obfu.raw("2 de la Crypte framboisée"),
                                                                     Obfu.raw("1 de la Véritable tombe de Tubz"),
                                                                     Obfu.raw("8 de la Cachette des conseillers"),
                                                                     Obfu.raw("Bonus")];
    private static var NAME_OFFSETS(default, never): Array<Int> = [55, 23, 20, 160];
    private static var INITIAL_VERTICAL_SPACE(default, never): Int = 180;
    private static var NAME_VERTICAL_SPACE(default, never): Int = 30;
    private static var DEATH_VERTICAL_SPACE(default, never): Int = 50;
    private static var VERTICAL_SPACE_BETWEEN_LOGS(default, never): Int = NAME_VERTICAL_SPACE + DEATH_VERTICAL_SPACE;
    private static var ALPHA_NOT_OPTION(default, never): Int = 20;

    private static function NbDeathString(nb_deaths: Int): String {
        return "" + nb_deaths + (nb_deaths <= 1 ? " mort" : " morts");
    }

    private function DrawLog(hf: Hf, game: GameMode): Void {
        var nb_drawn: Int = 0;
        var current_level_id: Int = game.world.currentId;
        if (current_level_id >= 1)
            ++nb_drawn;
        if (current_level_id >= 3)
            ++nb_drawn;
        if (current_level_id >= 5)
            ++nb_drawn;
        if (current_level_id >= 6)
            ++nb_drawn;

        var nightmare_text: MovieClip = SkyUI.CreateNormalTextSprite(hf, game, Obfu.raw("Cauchemar"), 120, 50, 200);
        nightmare_text._alpha = game.fl_nightmare ? 100 : ALPHA_NOT_OPTION;
        var unstable_text: MovieClip = SkyUI.CreateNormalTextSprite(hf, game, Obfu.raw("Explosifs instables"), 70, 90, 200);
        unstable_text._alpha = game.fl_bombExpert ? 100 : ALPHA_NOT_OPTION;
        map_screen_additional_sprites.push(nightmare_text);
        map_screen_additional_sprites.push(unstable_text);

        for (i in 0...nb_drawn) {
            map_screen_additional_sprites.push(SkyUI.CreateNormalTextSprite(hf, game, LEVEL_NAMES[i], NAME_OFFSETS[i], INITIAL_VERTICAL_SPACE + i * VERTICAL_SPACE_BETWEEN_LOGS, 170));
            map_screen_additional_sprites.push(SkyUI.CreateNormalTextSprite(hf, game, NbDeathString(nb_deaths[i]), 163, INITIAL_VERTICAL_SPACE + NAME_VERTICAL_SPACE + i * VERTICAL_SPACE_BETWEEN_LOGS, 150));
        }
    }

    private function EraseLog(): Void {
        for (sprite in map_screen_additional_sprites)
            sprite.removeMovieClip();
        map_screen_additional_sprites = [];
    }

    @:diExport
    public var force_ability_to_set_darkness_from_begin(default, null): IPatch;
    @:diExport
    public var death_reset_and_stats(default, null): IPatch;
    @:diExport
    public var map_log(default, null): IPatch;
    @:diExport
    public var remove_map_log(default, null): IPatch;

    @:diExport
    public var addBombExpert(default, null): IAction;
    @:diExport
    public var noRemainingLives(default, null): IAction;

    public function new(): Void {
        nb_deaths = [0, 0, 0, 0];
        map_screen_additional_sprites = [];

        // TODO: Remove when there is a GameParams.
        this.force_ability_to_set_darkness_from_begin = Ref.auto(GameMode.initGame).before(function(hf: Hf, self: GameMode): Void {
            hf.Data.MIN_DARKNESS_LEVEL = 0;
        });

        this.death_reset_and_stats = Ref.auto(Player.killPlayer).before(function(hf: Hf, self: Player): Void {
            switch (self.game.world.currentId) {
            case 1:
                ++this.nb_deaths[0];
            case 3:
                ++this.nb_deaths[1];
            case 5:
                ++this.nb_deaths[2];
            case 6:
                ++this.nb_deaths[3];
            }

            var bombs : Array<Entity> = self.game.getList(hf.Data.BOMB);
            for (bomb in bombs)
                bomb.destroy();

            var projectiles : Array<Entity> = self.game.getList(hf.Data.SHOOT);
            for (projectile in projectiles)
                projectile.destroy();

            if (self.lives < 1)
                self.lives = 1;
        });

        this.map_log = Ref.auto(GameMode.onMap).replace(function(hf: Hf, self: GameMode): Void {
            /* The original part of the method that we keep. */
            self.fl_pause = true;
            self.lock();
            self.world.lock();
            if (!self.fl_mute)
                self.setMusicVolume(0.5);

            /* Add our own sprites. */
            SkyUI.PushDarknessAndLightSources(self, 75);
            DrawLog(hf, self);
        });

        this.remove_map_log = Ref.auto(GameMode.onUnpause).after(function(hf: Hf, self: GameMode): Void {
            /* Remove our sprites and restore what we changed. */
            SkyUI.PopDarknessAndLightSources(self);
            EraseLog();
        });

        addBombExpert = new Bombexpert().action;
        noRemainingLives = new NoRemainingLives();
    }
}